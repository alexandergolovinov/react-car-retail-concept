import { createClient } from 'contentful'
/*const API_KEY = "saJqB-lLlWNKxG6Em7kui82HU1C92JD457qIWSaTNTU"
const SPACE_ID = "xd8y5qe1attb"*/
const OLD_SPACE_ID = "xd8y5qe1attb"
const OLD_API_KEY = "saJqB-lLlWNKxG6Em7kui82HU1C92JD457qIWSaTNTU"
export async function getEntriesBasedOnEntryName(entryName, userLanguage) {
    const entries = await createClient({
      space: OLD_SPACE_ID,
      accessToken: OLD_API_KEY,
    }).getEntries({locale: userLanguage}).catch(err => console.log(err));
    return entries.items.filter(item =>(item.sys.contentType.sys.id === entryName))
}

export function modifyLanguage (la) {
   return (la === 'fi')? "fi-FI" : "en-US"
}
