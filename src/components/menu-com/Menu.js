import React, {Component} from 'react';
import {Nav, Image} from 'react-bootstrap'
import ShortBackground from '../../images/short-background.png'
import {GiHamburgerMenu} from 'react-icons/gi'
import {MdPerson} from 'react-icons/md'
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'
import detectBrowserLanguage from 'detect-browser-language'

var styledMenu = {
   paddingTop: "0rem",
   paddingRight: "0rem",
   marginRight: "0.5rem !important"
}
var styledNav = {
  position: "relative",
  textAlign: "center",
  left: "60%"
}

var styledIcon = {
  position: "relative",
  left: "60%",
  marginLeft: "3%",
  marginTop: "2%"
}

var styledSpan = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
}

var styledLogo = {
  position: "absolute",
  color: "#000",
  fontWeight: "bold",
  fontSize: "2rem",
  left: "-15%",
  marginTop: "2%"
}
export default class Menu extends Component {
  constructor(){
     super()
     this.state = {
        menuArr: [],
        entryName: 'menu'
     }
  }

  componentDidMount(){
     const userLanguage = modifyLanguage(detectBrowserLanguage())
     getEntriesBasedOnEntryName(this.state.entryName, userLanguage)
     .then(res => {
        this.setState({
           menuArr: res
        })
     })
     .catch(err => console.log(err))
  }

  render() {
     return (
       <Nav className="justify-content-right" style={styledMenu}>
         <Nav.Item>
           <span style={styledLogo}>Logo</span>
         </Nav.Item>
         {
           this.state.menuArr.map( (menu,i) => {
              return (
                <Nav.Item key={i} style={styledNav}>
                  <Image src={ShortBackground} fluid/>
                  <span style={styledSpan}>
                    <Nav.Link style={{fontSize: "0.6rem", color: "#FFF"}} href={menu.fields.link}>
                       {menu.fields.name}
                    </Nav.Link>
                  </span>
                </Nav.Item>
              )
           })
         }
         <Nav.Item style={styledIcon}>
           <MdPerson size={32}/>
         </Nav.Item>
         <Nav.Item style={styledIcon}>
           <GiHamburgerMenu size={32}/>
         </Nav.Item>
       </Nav>
     )
  }
}
