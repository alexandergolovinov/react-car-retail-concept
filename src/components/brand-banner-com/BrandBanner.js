import React, {Component} from 'react';
import {getEntriesBasedOnEntryName} from "../../services/CommonService";
import './BrandBanner.css';
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";
import Col from "react-bootstrap/Col";

export default class BrandBanner extends Component {
    constructor() {
        super();
        this.state = {
            brandImages: []
        }
    }

    componentDidMount() {
        let entryType = "brandType";
        getEntriesBasedOnEntryName(entryType)
            .then(res => {
                this.setState({
                    brandImages: res[0].fields.brandImages
                });
                console.log(this.state.brandImages)
            }).catch(err => console.log(err))
    }

    render() {
        return (
            <Container className="la-brand-banner">
                <Row className="justify-content-center" style={{textAlign: "center"}}>
                   <Col>
                    {
                     this.state.brandImages.map( (image, i) => {
                       return (
                          <Image className="px-1" key={i} src={image.fields.file.url} alt="..."/>
                       )
                     })
                    }
                  </Col>
                </Row>
            </Container>
        )
    }
}
