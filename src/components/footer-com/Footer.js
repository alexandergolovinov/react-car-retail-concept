import React, {Component} from 'react';
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'
import Reservation from './Reservation'
import { Container, Row } from 'react-bootstrap';
import detectBrowserLanguage from 'detect-browser-language'
import './Footer.css'

export default class Footer extends Component {
  constructor() {
     super();
     this.state = {
       reservations : []
     };
   }

  componentDidMount() {
    const userLanguage = modifyLanguage(detectBrowserLanguage())
      getEntriesBasedOnEntryName("footer", userLanguage)
      .then(res => {
        this.setState({
          reservations: res
        });
      })
      .catch(err => console.log(err))
  }

  render() {
     return (
       <Container className="footer-container" >
          <Row className="justify-content-center">
              {this.state.reservations.map( (reservation,i) => {
                 return (<Reservation key={i} reservation={reservation} />)
              })}
          </Row>
      </Container>
     )
  }
}
