import React, {Component} from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';
import {GiCarKey, GiSteeringWheel, GiAutoRepair, GiBackwardTime} from 'react-icons/gi'
import './Reservation.css'

export default class Reservation extends Component {

renderSwitch(id) {
   switch (id) {
       /*VARAA MYYJÄLLE*/
       case "7edE6I0r3pqxIzum1zsSbG":
         return <GiBackwardTime size={32} />
       /*VARAA KOEAJO*/
       case "25HddhOOK4b0vAZVrOVZtY":
         return <GiSteeringWheel size={32} />
       /*VARAA VUOKRA-AUTO*/
       case "535t2mUZ1Fui315TQefX8h":
         return <GiCarKey size={32} />
       /*VARAA HUOLTO*/
       case "5s1ATs2knMAQeMBR9yRVDq":
         return <GiAutoRepair size={32} />
       default:
         return ""
     }
}
   render() {
      const reservationObj = this.props.reservation
      return (
         <Col className="mt-5">
             <Container>
                <Row>
                  <Col>
                    {
                       this.renderSwitch(reservationObj.sys.id)
                    }
                  </Col>
                </Row>
                <Row>
                  <Col>
                      <Nav.Link className="reservation-nav-link">
                          {(reservationObj)? reservationObj.fields.service : "Not found"}
                      </Nav.Link>
                  </Col>
                </Row>
             </Container>
         </Col>
      )
   }
}
