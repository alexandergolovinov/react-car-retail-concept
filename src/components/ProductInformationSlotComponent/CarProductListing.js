import React, { Component } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import CarProduct from './CarProduct'
import detectBrowserLanguage from 'detect-browser-language'
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'
import Car1 from '../../images/737623_1.png'
import Car2 from '../../images/758859_2.png'
import Car3 from '../../images/729722_15.png'

export default class CarProductListing extends Component {
   constructor() {
      super()
      this.state = {
         entryName: "footer",
         title: "Uusimmat vaihtoautot",
         products: [{
           carImage: Car1,
           title: "FORD MUSTANG",
           spec: "5,0 V8 GT 418hv A6 Fastback",
           buttonLabel: "2015   11 000 km"
         },{
           carImage: Car2,
           title: "OPEL CORSA",
           spec: "5-ov EXCITE 1,0T ecoFLEX Start/S…",
           buttonLabel: "2018   11 500 km"
         },{
           carImage: Car3,
           title: "MERCEDES-BENZ",
           spec: "220 d 4Matic A Business AMG, 1 o…",
           buttonLabel: "2016   32 500 km"
         }]
      }
   }

   componentDidMount() {
     const userLanguage = modifyLanguage(detectBrowserLanguage())
       console.log('Fetch data for CarProductListing component...')
       getEntriesBasedOnEntryName(this.state.entryName, userLanguage)
       .then(res => {
         /*this.setState({
            products: res
         })*/
       })
       .catch(err => console.log(err))
   }

   render() {
        return (
           <Container>
              <Row>
                  <Col><h3>{ this.state.title }</h3></Col>
              </Row>
              <Row>
                     {
                     this.state.products.map ( (product, i ) => {
                        return (
                            <Col sm={4} md={4} className="p-auto" key={i}>
                                <CarProduct data={product} />
                           </Col>
                        )
                     })
                     }
              </Row>
           </Container>
      )
    }
}
