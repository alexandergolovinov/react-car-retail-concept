import React, {Component} from 'react';
import {Container, Row, Col, Image} from 'react-bootstrap'
import {getEntriesBasedOnEntryName, modifyLanguage} from "../../services/CommonService";
import detectBrowserLanguage from "detect-browser-language";

export default class CarAdvertisement extends Component {
    constructor() {
        super();
        this.state = {
            advertisement: []
        }
    }

    componentDidMount() {
        const userLanguage = modifyLanguage(detectBrowserLanguage())
        getEntriesBasedOnEntryName("news", userLanguage)
            .then(res => {
                this.setState({
                    advertisement: res.slice(0, 1)
                });
                console.log("Ads", res.slice(0, 1))
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <Container>
                <Row>
                     {this.state.advertisement.map((adv, i) => {
                         return (
                             <Col key={i} style={{maxWidth: "100%"}}>
                                 <h3>{adv.fields.compnentHeader}</h3>
                                 <Image src={adv.fields.media.fields.file.url} style={{maxWidth: "100%"}}/>
                                 <h4 className="pt-4">{adv.fields.title}</h4>
                            </Col>
                         )
                     })}
                </Row>
            </Container>
        )
    }
}
