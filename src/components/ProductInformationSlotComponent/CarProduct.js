import React, { Component } from 'react'
import { Card, Nav } from 'react-bootstrap'

export default class CarProduct extends Component {
    render(){
      const product = this.props.data
       return (
         <Card style={{ maxWidth: '100%', border: "none", padding: 0}}>
           <Card.Img variant="top" src={product.carImage} />
           <Card.Body>
             <Card.Title>
               <Nav.Link href="#" style={{padding: 0}}>
                 {product.title}
               </Nav.Link>
             </Card.Title>
             <Card.Text>
               <span>{product.spec}</span>
             </Card.Text>
             <Card.Text style={{textAlign: "left"}}>
                <span>{product.buttonLabel}</span>
                <span style={{float: "right", fontWeight: "bold", fontSize: "2rem"}}>{"586 €/kk"}</span>
             </Card.Text>
           </Card.Body>
         </Card>
       )
    }
}
