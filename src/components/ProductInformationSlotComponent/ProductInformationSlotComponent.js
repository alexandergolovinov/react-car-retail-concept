import React, {Component} from 'react';
import {Container, Row, Col} from 'react-bootstrap'
import CarAdvertisement from "./CarAdvertisement";
import CarProductListing from './CarProductListing'
import './CarAdvertisement.css';
import CustomButton from '../common-com/CustomButton'

export default class ProductInformationSlotComponent extends Component {

    render() {
        return (
            <Container className="pb-5">
                <Row>
                    <Col sm={4} md={4}>
                        <CarAdvertisement />

                    </Col>
                    <Col sm={8} md={8}>
                        <CarProductListing />
                    </Col>
                </Row>
                <Row>
                  <Col sm={4} md={4}>
                    <CustomButton text="Katso kaikki ajankotaiset edut" />
                  </Col>
                  <Col sm={8} md={8}>
                    <CustomButton text="Katso kaikki vaihtoautot" />
                  </Col>
                </Row>
            </Container>
        )
    }
}
