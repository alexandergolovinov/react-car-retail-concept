import React, {Component} from 'react';
import {Container, Row} from "react-bootstrap";
import {getEntriesBasedOnEntryName} from "../../services/CommonService";
import Col from "react-bootstrap/Col";
import './HomeAction.css';

export default class HomeAction extends Component {
    constructor() {
        super();
        this.state = {
            actionData: []
        }
    }

    componentDidMount() {
        let entryType = "homeCallAction";
        getEntriesBasedOnEntryName(entryType).then(response => {
                this.setState({
                    actionData: response
            });
            console.log(this.state.actionData);
        }).catch(err => console.log(err));
    }

    render() {
        return (
            <Container className="la-home-action">
                {this.state.actionData.map((val, i) => {
                    return (
                        <Row key={i}>
                            <Col className="la-home-action-text">
                                <p>{val.fields.text}</p>
                            </Col>
                            <Col xs lg={4}>
                                <div className="la-home-action-btn">
                                    <a href={val.fields.url}>
                                        <span>LUE LISÄÄ</span>
                                    </a>
                                </div>
                                <div className="la-home-small-btn-icon">
                                </div>
                            </Col>
                        </Row>
                    )
                })}
            </Container>
        )
    }
}
