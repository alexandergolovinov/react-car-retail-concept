import React, {Component} from 'react';
import {Container, Row, Col, Nav, Image} from 'react-bootstrap'
import ShortBackground from '../../images/short-background.png'
import {GiHamburgerMenu} from 'react-icons/gi'
import {MdPersonOutline} from 'react-icons/md'
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'
import detectBrowserLanguage from 'detect-browser-language'
import CustomCarousel from '../carousel-com/CustomCarousel'
import LanguageDropdown from './LanguageDropdown'
import './Navigation.css'

export default class Navigation extends Component {
  constructor(){
     super()
     this.toggleMenuDropdown = this.toggleMenuDropdown.bind(this)
     this.state = {
        menuArr: [],
        entryName: 'menu',
        isShow: false
     }
  }

  componentDidMount(){
     const userLanguage = modifyLanguage(detectBrowserLanguage())
     getEntriesBasedOnEntryName(this.state.entryName, userLanguage)
     .then(res => {
        console.log("Menu.js ",res)
        this.setState({
           menuArr: res
        })
     })
     .catch(err => console.log(err))
  }

  toggleMenuDropdown (e) {
     this.setState({
        isShow: !this.state.isShow
     })
  }

  render() {
     return (
       <Container>
          <Row className="navigation-row" id="navigationRow">
              <Col md={3} xs={3} >
                  <Nav>
                    <Nav.Item>
                       <span className="navigation-logo">Logo</span>
                    </Nav.Item>
                  </Nav>
              </Col>
              <Col md={6} xsHidden>
                <Nav className="navigation-menu">
                  {
                    this.state.menuArr.map( (menu,i) => {
                      return (i === 0)?
                         (
                          <Nav.Item key={i} className="navigation-first-nav">
                            <Image src={ShortBackground} />
                            <span className="navigation-span">
                              <Nav.Link className="navigation-nav-link" href={menu.fields.link}>
                                 {menu.fields.name}
                              </Nav.Link>
                            </span>
                          </Nav.Item>
                        ):(
                          <Nav.Item key={i} className="navigation-second-nav">
                            <Image src={ShortBackground} />
                            <span className="navigation-span">
                              <Nav.Link className="navigation-nav-link" href={menu.fields.link}>
                                 {menu.fields.name}
                              </Nav.Link>
                            </span>
                          </Nav.Item>
                        )
                      })
                  }
                </Nav>
              </Col>
              <Col md={3} xs={3}>
                 <Nav>
                   <Nav.Item className="navigation-icon">
                     <MdPersonOutline size={32} />
                   </Nav.Item>
                   <Nav.Item className="navigation-icon">
                      <GiHamburgerMenu size={32} onClick={() => this.toggleMenuDropdown()} />
                      <LanguageDropdown show={this.state.isShow} />
                   </Nav.Item>
                 </Nav>
              </Col>
          </Row>
          <Row>
              <Col>
                 <CustomCarousel />
              </Col>
          </Row>
       </Container>
     )
  }
}
