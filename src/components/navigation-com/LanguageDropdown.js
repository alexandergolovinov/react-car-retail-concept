import React, { Component } from 'react';
import { Dropdown, ListGroup } from 'react-bootstrap'
import {GiHamburgerMenu} from 'react-icons/gi'
import './LanguageDropdown.css'

export default class LanguageDropdown extends Component {
   constructor() {
      super()
      this.state = {
         languages: ["FI", "EN"]
      }
    }

    render() {
       return (
          <ListGroup className={ (this.props.show)? "ul-show" : "ul-hide" }>
            {
              this.state.languages.map ( (lang, i) => {
                  return (i === 0) ? (
                    <ListGroup.Item key={i} > {lang} </ListGroup.Item>
                  ) : (
                    <ListGroup.Item key={i} > {lang} </ListGroup.Item>
                  )

              })
            }
          </ListGroup>
       )
    }
}
