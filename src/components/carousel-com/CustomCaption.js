import React, {Component} from 'react';
import {Card, Carousel, Image, Nav} from 'react-bootstrap'
import LongBackground from '../../images/long-background.png'
import { FaChevronRight } from 'react-icons/fa'

var styledTitle = {
  position: "relative",
  clear: "both",
  display: "inline-block",
  overflow: "hidden",
  whiteSpace: "nowrap",
  fontSize: "2rem"
}

var styledSpan = {
  position: "absolute",
  top: "50%",
  left: "60%",
  transform: "translate(-50%, -50%)",
  clear: "both",
  display: "inline-block",
  overflow: "hidden",
  whiteSpace: "nowrap"
}

var styledNav = {
  fontSize: "1rem",
  color: "#FFF",
  fontWeight: "bold"
}

var styledIcon = {
  position: "absolute",
  top: "50%",
  left: "15%",
  transform: "translate(-50%, -50%)",
  fontSize: "1rem",
  color: "#FFF",
  clear: "both",
  display: "inline-block",
  overflow: "hidden",
  whiteSpace: "nowrap"
}

var styledImage = {
   height: "130%",
   width: "120%"
}

export default class CustomCaption extends Component {
    render () {
        const  styledCaption = this.props.type
        const  caption = this.props.caption
        return (
          <Carousel.Caption style={styledCaption}>
            <Card style={ styledCaption }>
             <Card.Body>
               <Card.Title style={styledTitle}><h3 style={{ color: caption.fields.color}}>{ caption.fields.title } </h3></Card.Title>
               <Card.Text>
                 <h6 style={{textAlign: "left"}}>{ caption.fields.h5Text }</h6>
                 <h4 style={{textAlign: "left"}}>{ caption.fields.h4Text }</h4>
               </Card.Text>
               <Card.Text style={{position: "relative"}}>
                 <Image src={LongBackground} style={styledImage} />
                 <FaChevronRight size={18} style={styledIcon}/>
                 <span style={styledSpan}><Nav.Link href={caption.link} style={styledNav}>{ caption.fields.buttonLabel }</Nav.Link></span>
               </Card.Text>
             </Card.Body>
            </Card>
          </Carousel.Caption>
        )
    }
}
