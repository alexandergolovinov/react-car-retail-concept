import React, {Component} from 'react';
import {Carousel, Image} from 'react-bootstrap'
import CustomCaption from './CustomCaption'
import detectBrowserLanguage from 'detect-browser-language'
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'

var styledCarousel = {
  position: "relative",
  width: "100%",
  maxHeight: "100%",
  marginLeft: "auto",
  marginRight: "auto"
}

export default class CustomCarousel extends Component {

  constructor() {
     super()
     this.state = {
        entryName: "caption",
        captions: [],
        rightPosition: "60%",
        leftPosition: "20%",
        leftDisplay: "",
        rightDisplay: ""
     }
  }

  componentDidMount() {
    const userLanguage = modifyLanguage(detectBrowserLanguage())
      console.log('Fetch data for CustomerCarousel component...')
      getEntriesBasedOnEntryName(this.state.entryName, userLanguage)
      .then(res => {
        this.setState({
           captions: res,
           rightPosition: res[1].fields.space+"%",
           rightDisplay: (res[1].fields.hidden === "true")? "none" : "",
           leftPosition: res[0].fields.space+"%",
           leftDisplay: (res[0].fields.hidden === "true")? "none" : ""
        })
      })
      .catch(err => console.log(err))
  }

  getStyle = (pos) => {
     if (pos === "right") {
        return {
           display: this.state.rightDisplay,
           width: '18rem',
           background: 'none',
           color: "#000",
           border: "none",
           position: "absolute",
           top: "50%",
           left: this.state.rightPosition,
           transform: "translate(-50%, -50%)"
        }
     } else {
        return {
           display: this.state.leftDisplay,
           width: '18rem',
           background: 'none',
           color: "#000",
           border: "none",
           position: "absolute",
           top: "50%",
           left: this.state.leftPosition,
           transform: "translate(-50%, -50%)"
        }
     }
  }

  render() {
     console.log("After CustomerCarousel rendering....", this.state.captions)
     return (this.state.captions.length > 0) ? (
       <Carousel style={styledCarousel} interval="10000" pauseOnHover={true}>
         <Carousel.Item>
           <Image
             className="d-block w-100"
             src={this.state.captions[0].fields.carouselImage.fields.file.url}
             alt="Right caption"
             fluid
           />
         <CustomCaption caption={this.state.captions[0]} type={this.getStyle(this.state.captions[0].fields.position)}/>
         </Carousel.Item>
         <Carousel.Item>
           <Image
             className="d-block w-100"
             src={this.state.captions[1].fields.carouselImage.fields.file.url}
             alt="Left caption"
             fluid
           />
         <CustomCaption caption={this.state.captions[1]} type={this.getStyle(this.state.captions[1].fields.position)}/>
         </Carousel.Item>
       </Carousel>
     ) : ("Data fetching error!")
  }
}
