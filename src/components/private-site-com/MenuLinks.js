import React, {Component} from 'react';
import {Container, Row} from 'react-bootstrap'
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'
import detectBrowserLanguage from 'detect-browser-language'
import MenuLinksChild from './MenuLinksChild'


export default class MenuLinks extends Component {
  constructor() {
      super();
      this.state = {
          entryName: "menuLinks",
          menuLinks: []
      }
  }
    componentDidMount() {
      const userLanguage = modifyLanguage(detectBrowserLanguage())
      getEntriesBasedOnEntryName(this.state.entryName, userLanguage)
      .then(res => {
         this.setState({
           menuLinks: res
         })
      })
      .catch(err => console.log(err))
    }

   render() {
      return (
         <Container>
             <Row className="justify-content-md-center">
                {
                  this.state.menuLinks.map( (menu, i) => {
                      return (
                         <MenuLinksChild key={i} menu={menu} />
                      )
                  })
                }
             </Row>
         </Container>
      )
   }
}
