import React, {Component} from 'react';
import {Container, Row, Card} from 'react-bootstrap'
import PersonalInfo from './PersonalInfo'
import CustomerListing from './CustomerListing'
import MenuLinks from './MenuLinks'

var styledPrivateSite = {
   height: "100%",
   width: "100%"
}
export default class PrivateSite extends Component {

  render() {
     return (
        <Card className="p-5 mt-3" bg="light" style={styledPrivateSite}>
          <Container>
             <Row className="justify-content-sm-center mt-2">
                  <PersonalInfo />
                  <CustomerListing />
                  <MenuLinks />
             </Row>
          </Container>
        </Card>
     )
  }
}
