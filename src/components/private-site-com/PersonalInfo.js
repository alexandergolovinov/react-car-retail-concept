import React, {Component} from 'react';
import {Container, Row, Col} from 'react-bootstrap'
import {MdPerson} from 'react-icons/md'
import { getEntriesBasedOnEntryName, modifyLanguage } from '../../services/CommonService'
import PersonalInfoChild from './PersonalInfoChild'
import detectBrowserLanguage from 'detect-browser-language'

var styledIcon = {
    color: "#4B4B4B"
}
export default class PersonalInfo extends Component {
  constructor(){
     super();
     this.state = {
        entryName: 'personalInfo',
        customers: ["Allu","Kerttu"],
        dataArr: []
     }
  }

  componentDidMount(){
     const userLanguage = modifyLanguage(detectBrowserLanguage())
     getEntriesBasedOnEntryName(this.state.entryName, userLanguage)
     .then(res => {
        const dataArr = ['','','','',''];
        res.map( item => {
          switch (item.sys.id) {
            /*Omat tiedot*/
            case '7zxlFPkhyqbxRJV2s9fwjT':
              dataArr[0] = item.fields.text
              break;
            /*Renkaiden kausivaihtosopimus*/
            case "5cB3vnhQ7Wi5Irn0Kjrauq":
              dataArr[4] = item.fields.text
              break;
            /*Extra huoltosopimus*/
            case "2yeXBslC5HSNOyaPQ6qQ2q":
              dataArr[3] = item.fields.text
              break;
            /*Leasing sopimus*/
            case "4lfsI2aSOhdF8yR1QfIKlw":
              dataArr[2] = item.fields.text
              break;
            /*Autoilija*/
            case "4aJOlQ4yAQg3gEHVZKrznl":
              dataArr[1] = item.fields.text
              break;
            default:
           }
        })
        this.setState({
           dataArr: dataArr
         })
     })
     .catch(err => console.log(err))
  }

  render() {
     return (
        <Col sm={4} className="text-left">
            <Container>
                <Row>
                    <Col>
                       <MdPerson className="d-sm-inline" size={48} style={styledIcon}/>
                       <h5 className="d-sm-inline">{this.state.dataArr[0]}</h5>
                    </Col>
                </Row>
                <Row className="justify-content-center">
                   <Col>
                     {
                       this.state.customers.map( (customer, i) => {
                          return (<PersonalInfoChild key={i}  customer={customer +' '+ this.state.dataArr[1]} data={this.state.dataArr.filter((item, i) => i >= 2)} index={i} />)
                       })
                     }
                   </Col>
                </Row>
            </Container>
        </Col>
     )
  }
}
