import React, {Component} from 'react';
import {Nav} from 'react-bootstrap'

var styledNav = {
   verticalAlign: "middle"
}

var styledUl = {
  li: {
    listStyleType: "none",
    textAlign: "left !important"
  }
}

var styledDot = {
  height: "5px",
  width: "5px",
  backgroundColor: "#000",
  borderRadius: "50%",
  display: "inline-block",
  margin: "auto"
}

var styledTextSpan = {
  margin: "1rem 0",
  paddingLeft: "0.5rem"
}

var styledHr = {
  backgroundColor: "#BEBEBE",
  border: "1px solid #BEBEBE"
}

export default class PersonalInfoChild extends Component {
  render() {
     const customerName = this.props.customer
     const dataArr = this.props.data
     const index = this.props.index
     return (
       <ul style={styledUl.li}>
         <h4>{customerName}</h4>
         {
           dataArr.map( (data,i) => {
                return (
                  <li key={i}>
                     <Nav.Link style={styledNav} >
                        <span style={styledDot}></span>
                        <span style={styledTextSpan}>{data}</span>
                     </Nav.Link>
                  </li>
                )
            })
          }
          <hr style={(index === 0)? styledHr : {display: "none"}} />
      </ul>
    )
  }
}
