import React, {Component} from 'react';
import {Col} from 'react-bootstrap'
import CustomButton from '../common-com/CustomButton'

export default class MenuLinksChild extends Component {

   render(){
     const menu = this.props.menu.fields.name
     return(
       <Col md="4" sm="4" className="mb-3 py-2">
          <CustomButton text={menu} />
       </Col>

     )
   }
}
