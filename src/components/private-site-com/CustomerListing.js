import React, {Component} from 'react';
import {Container, Row, Col} from 'react-bootstrap'
import CustomerListingChild from './CustomerListingChild'
import mercedesA from '../../images/Mercedes_A.png';
import Mercedes_CLA from '../../images/Mercedes_CLA.png';

export default class CustomerListing extends Component {
   constructor(){
      super();
      this.state = {
         customers: [{
            customerName: "Allu Autoilijan auto",
            carImage: Mercedes_CLA,
            car: {
               carModel: "Mercedes-Benz CLA Coupe",
               previewService: "Edellinen huolto: 30 000 km",
               dateService: "21. Kesäkuu 2019",
               nextService: "Seuraava huolto: 60 000 km/1. vuosi",
               estimatedTime: "Arvioitu 12. Tammikuuta 2020"
            }
         }, {
            customerName: "Allu Autoilijan auto",
            carImage: mercedesA,
            car: {
               carModel: "Mercedes-Benz CLA Coupe",
               previewService: "30 000 km",
               dateService: "21. Kesäkuu 2019",
               nextService: "60 000 km/1. vuosi",
               estimatedTime: "12. Tammikuuta 2020"
            }
         }]
      }
   }
  render() {
     return (
       <Col sm={8}>
          <Container>
             <Row>
                {this.state.customers.map( (customer,i) => {
                    return (
                       <CustomerListingChild key={i} data={customer}/>
                    )
                })}
             </Row>
          </Container>
       </Col>
     )
  }
}
