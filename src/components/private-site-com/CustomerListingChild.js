import React, {Component} from 'react';
import {Col, Image} from 'react-bootstrap'

var styledImage = {
   height: "50%",
   width: "80%",
   marginBottom: "2.5rem",
   marginTop: "1rem"
}

var styledUl = {
  li: {
    listStyleType: "none",
    fontWeight: "bold"
  }
}
export default class CustomerListingChild extends Component {
    render(){
       const customer = this.props.data
       return (
         <Col xs={12} sm={12} md={6}>
             <ul style={styledUl.li}>
               <h3 className="mb-2">{customer.customerName}</h3>
               <Image style={styledImage} src={customer.carImage} alt="car image..." fluid/>
               <li className="pb-2">{customer.car.carModel}</li>
               <li className="pb-2">{customer.car.previewService}</li>
               <li className="pb-2">{customer.car.dateService}</li>
               <li className="pb-2">{customer.car.nextService}</li>
               <li className="pb-2">{customer.car.estimatedTime}</li>
             </ul>
         </Col>
       )
    }
}
