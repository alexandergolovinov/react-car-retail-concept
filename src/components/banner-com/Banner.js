import React, {Component} from 'react'
import styled from 'styled-components'
import {Container} from 'react-component'

const StyledContainer = styled.Container`
    height: '200px',
    width: '100%',
    background-color: '#37BC9B',
    content: 'Articles here'
`
export default class Banner extends Component {
  render() {
     return (
        <StyledContainer>
        </StyledContainer>
     )
  }
}
