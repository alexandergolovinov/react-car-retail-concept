import React, {Component} from 'react';
import Image from 'react-bootstrap/Image'
import {MdKeyboardArrowRight} from 'react-icons/md'
import './Article.css';

export default class Articles extends Component {
    render() {
        const article = this.props.article;
        let templateRender = article.fields.template === "right" ? "la-article-item-left" : "la-article-item-right";
        return (
            <div className={templateRender}>
                <h4>{article.fields.title}</h4>
                <p>{article.fields.description}</p>
                <Image className="la-image" src={article.fields.media.fields.file.url}/>
                <div className="la-article-btn">
                    <a href="/">
                        <MdKeyboardArrowRight/><span>{article.fields.buttonText}</span>
                    </a>
                </div>
            </div>
        )
    }
}
