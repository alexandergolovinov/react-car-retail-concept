import React, {Component} from 'react';
import Article from "./Article";
import {getEntriesBasedOnEntryName, modifyLanguage} from "../../services/CommonService";
import detectBrowserLanguage from "detect-browser-language";
import {Container, Row, Col} from 'react-bootstrap';

let styledArticlesSlot = {
    padding: '10px 6px 10px 6px'
};

export default class Articles extends Component {
    constructor() {
        super();
        this.state = {
            articles: []
        };
    }

    componentDidMount() {
        const userLanguage = modifyLanguage(detectBrowserLanguage());
        getEntriesBasedOnEntryName("article", userLanguage)
            .then(res => {
                this.setState({
                    articles: res
                });
                console.log(res);
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <Container>
                <Row>
                    {this.state.articles.slice(0, 2).map((article, i) => {
                        return (
                            <Col xs="auto" sm="auto" md={6} key={i} style={styledArticlesSlot}>
                                <Article article={article}/>
                            </Col>
                        )
                    })}
                </Row>
            </Container>
        )
    }
}
