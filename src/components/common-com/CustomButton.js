import React, { Component } from 'react'
import './CustomButton.css'
import {MdKeyboardArrowRight} from 'react-icons/md'

export default class CustomButton extends Component {

    render(){
       const customText = this.props.text
       return (
         <div className="la-car-advertisement-btn">
             <a href="/">
                 <MdKeyboardArrowRight className="custom-button-span"/><span>{ customText }</span>
             </a>
         </div>
       )
    }
}
