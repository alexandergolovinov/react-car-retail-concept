import React, {Component} from 'react';
import Search from '../search-com/Search'
import BrandBanner from "../brand-banner-com/BrandBanner";
import PrivateSite from '../private-site-com/PrivateSite'
import Articles from '../articles-com/Articles'
import Footer from '../footer-com/Footer'
import HomeAction from "../home-action/HomeAction";
import Navigation from "../navigation-com/Navigation"
import {Container, Row} from 'react-bootstrap'
import ProductInformationSlotComponent from "../ProductInformationSlotComponent/ProductInformationSlotComponent";

export default class HomePage extends Component {
   constructor(){
      super()
   }
   componentDidMount(){
     window.addEventListener('scroll', this.handleScroll, true)
   }

   handleScroll(e) {
      const navigationRow = document.getElementById("navigationRow")
      if(window.pageYOffset > 0) {
         navigationRow.style.opacity = 0.1
      } else {
         navigationRow.style.opacity = 1
      }
   }

   render() {
      return (
         <Container>
            <Row>
              <Navigation />
              <Search />
              <HomeAction />
              <BrandBanner />
              <Articles />
              <ProductInformationSlotComponent />
              <PrivateSite />
              <Footer />
            </Row>
         </Container>
      )
   }
}
