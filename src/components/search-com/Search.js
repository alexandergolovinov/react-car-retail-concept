import React, {Component} from 'react';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import FormControl from "react-bootstrap/FormControl";
import {FiSearch} from "react-icons/fi"
import './Search.css';
import InputGroup from "react-bootstrap/InputGroup";

export default class Search extends Component {
    render() {
        return (
            <Container className="la-search">
                <Row>
                    <Col xs={10}>
                        <Row className="la-search-text">
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <FiSearch className="la-search-icon"/>
                                </InputGroup.Prepend>
                                <FormControl className="la-search-input" placeholder="HAE UUSIA JA KÄYTETTYJÄ AUTOJA"
                                             aria-label="Large" aria-describedby="inputGroup-sizing-sm"/>
                            </InputGroup>
                        </Row>
                    </Col>
                    <Col>
                        <div className="la-search-btn">
                            <a href="/">
                                <span>HAE</span>
                            </a>
                        </div>
                        <div className="la-search-small-icon">
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
