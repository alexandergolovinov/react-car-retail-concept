import React, {Component} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    BrowserRouter as Router,
    Route
} from "react-router-dom";
import HomePage from './components/home-page-com/HomePage'
export default class App extends Component {
  render() {
      return (
        <Router path="/">
          <Route path="/" exact component={() => <HomePage />}/>
        </Router>
      )
  }
}
